/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package escuela;

/**
 *
 * @author 52667
 */

public abstract class Empleado  {
    
    private int numEmpleado;
    private String nombre;
    private String domicilio;

    public Empleado(){
     
        this.numEmpleado = 0;
        this.nombre = "";
        this.domicilio = "";
    }
    
    public Empleado(int numEmpleado, String nombre, String domicilio) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.domicilio = domicilio;
    }

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
     
  
    
}
