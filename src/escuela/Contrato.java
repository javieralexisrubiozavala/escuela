/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package escuela;

/**
 *
 * @author 52667
 */
public class Contrato extends Empleado{
   
    private int claveContrato;
    private String puesto;
    private double impuestoISR;

    public Contrato(int claveContrato, String puesto, double impuesto, int numEmpleado, String nombre, String domicilio) {
        super(numEmpleado, nombre, domicilio);
        this.claveContrato = claveContrato;
        this.puesto = puesto;
        this.impuestoISR = impuesto;
    }

    public Contrato() {
        this.claveContrato = 0;
        this.puesto = "";
        this.impuestoISR = .16f;


    }

    public int getClaveContrato() {
        return claveContrato;
    }

    public void setClaveContrato(int claveContrato) {
        this.claveContrato = claveContrato;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public double getImpuestoISR() {
        return impuestoISR;
    }

    public void setImpuestoISR(double impuestoISR) {
        this.impuestoISR = impuestoISR;
    }
    
    
    
    
    
    }
    
    