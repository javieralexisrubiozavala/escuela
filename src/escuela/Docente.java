/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package escuela;

/**
 *
 * @author 52667
 */
public class Docente extends Empleado implements Impuesto  {
    int nivel; // Prescola, Primaria, Secundaria
    int horasLaboradas;
    float pagoHora; // pago por hora

    public Docente() {
        super();
        this.nivel = 0;
        this.horasLaboradas = 0;
        this.pagoHora = 0;
    }

    public Docente(int nivel, int horasLaboradas, float pagoHora, int numEmpleado, String nombre, String domicilio) {
        super(numEmpleado, nombre, domicilio);
        this.nivel = nivel;
        this.horasLaboradas = horasLaboradas;
        this.pagoHora = pagoHora;
    }
        

 
    public double calcularPagoAdicional() {
        double pagoBase = horasLaboradas * pagoHora;
        double porcentajeAdicional = 0;

        switch (nivel) {
            case 1:
                porcentajeAdicional = 0.35;
                break;
            case 2:
                porcentajeAdicional = 0.40;
                break;
            case 3:
                porcentajeAdicional = 0.50;
                break;
            default:
                System.out.println("Nivel no válido");
                return pagoBase;
        }

        double pagoAdicional = pagoBase * porcentajeAdicional;
        return pagoBase + pagoAdicional;
    }
    
    
    

    public double calcularTotal() {
        return calcularPagoAdicional() - calcularImpuesto();
    }
    
    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getHorasLaboradas() {
        return horasLaboradas;
    }

    public void setHorasLaboradas(int horasLaboradas) {
        this.horasLaboradas = horasLaboradas;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }
    

    @Override
    public float calcularImpuesto() {
        double pagoAdicional = calcularPagoAdicional();
        return (float) (pagoAdicional * 0.16);
    }
}


